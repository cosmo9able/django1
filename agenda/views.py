from django.http import HttpResponse

def index(request):
    return HttpResponse('Pagina inicial')

def hello(request):
    return HttpResponse('Olá mundo')

def somar(request, n1, n2):
	return HttpResponse(f'A soma de {n1} com {n2} é {n1 + n2}')
