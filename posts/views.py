from django.shortcuts import render
from django.http import HttpResponse

# Create your views here


def teste(request):
    lista = {
        'nome': 'Cosmo',
        'email': 'cosmo_moraes@mail.com',
        'idade': '22 anos'
    }

    return HttpResponse(f'Olá mundo {lista}')
