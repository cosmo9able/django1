from django.db import models
from clientes.models import Cliente
from django.utils import timezone
# Create your models here.

class Post(models.Model):
    titulo = models.CharField(max_length=25, blank=False)
    texto = models.CharField(max_length=300, blank=False)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return f'{self.titulo} | {self.cliente.nome.title()}'

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    texto = models.CharField(max_length=150, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.texto
