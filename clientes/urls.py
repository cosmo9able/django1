from django.urls import path

from .views import *

urlpatterns = [
	path('', index),
	path('template', meu_template),
	path('cosmo', cosmo)
]