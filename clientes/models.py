from django.db import models

# Create your models here.

class Cpf(models.Model):
    numero = models.CharField(max_length=10)
    emissao = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.numero} / {self.emissao}'

class Grupo(models.Model):
    titulo = models.CharField(max_length=20, unique=True)
    descricao = models.CharField(max_length=100)
    salario = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.titulo.title()} / {self.descricao.title()} / R${self.salario:>.2f}'

class Cliente(models.Model):
    nome = models.CharField(max_length=70)
    email = models.EmailField(max_length=45)
    endereco = models.CharField(max_length=100)
    cpf = models.OneToOneField(Cpf, on_delete=models.CASCADE)
    imagem = models.ImageField(upload_to='cliente', blank=True)
    grupo = models.ManyToManyField(Grupo)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nome.title()

class Telefone(models.Model):
    numero = models.CharField(max_length=50)
    descricao = models.CharField(max_length=20)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.cliente.nome.title()} | {self.numero} | {self.descricao.capitalize()}'

class CarroFabricante(models.Model):
    nome = models.CharField(max_length=15)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nome.title()

class CarroModelo(models.Model):
    nome = models.CharField(max_length=20)
    carro_fabricante = models.ForeignKey(CarroFabricante, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.carro_fabricante.nome.title()} - {self.nome.title()}'

class Carro(models.Model):
    ano = models.DateField()
    modelo = models.ForeignKey(CarroModelo, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.modelo.carro_fabricante.nome.title()} / {self.modelo.nome.title()} / {self.ano}'