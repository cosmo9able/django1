from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Cliente)
admin.site.register(Telefone)
admin.site.register(CarroFabricante)
admin.site.register(CarroModelo)
admin.site.register(Carro)
admin.site.register(Cpf)
admin.site.register(Grupo)