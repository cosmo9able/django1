from django.shortcuts import render
from django.http import HttpResponse
from random import choice

def index(request):
	return HttpResponse('oLa mundo')

def meu_template(request):
	return render(request, 'teste.html')

def cosmo(request):
	clientes = [
		{'nome':'Ritinha Moraes', 'idade':21},
		{'nome':'Cosmo André', 'idade':22},
		{'nome':'Ryan Braz', 'idade':11},
		{'nome':'Cecilia Braz', 'idade':16},
		{'nome':'Wanderson Boiola', 'idade':24},
		{'nome':'Rita Braz', 'idade':'xx'}
	]


	return render(request, 'clientes/cosmo.html', {
		'cliente':choice(clientes)
	})
